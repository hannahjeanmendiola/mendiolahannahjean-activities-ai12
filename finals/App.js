import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
import { Alert, Modal, StyleSheet, Text, Pressable, View, Button } from "react-native";
import { useState } from "react";
import { Input, Icon, FAB } from 'react-native-elements';
import NoteLists from './src/note/calendar';
import DateTimePickerModal from "react-native-modal-datetime-picker";


LocaleConfig.locales['fr'] = {
  monthNames: [
    '	January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ],
  monthNamesShort: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'],
  dayNames: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  dayNamesShort: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  today: "Today"
};
LocaleConfig.defaultLocale = 'fr';
export default function App() {
  const [modalIsVisible, setModalIsVisible] = useState(false)
  let [currentNote, setCurrentNote] = useState({})
  let [markedDates, setMarkedDates] = useState({})
  let [allNotes, setAllNotes] = useState([])
  const [currentDate, setCurrentDate] = useState("")
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.log("A date has been picked: ", date.toString().split(" ")[4]);
    setCurrentNote({
      ...currentNote,
      time: date.toString().split(" ")[4]
    })
    hideDatePicker();
  };
  return (
    <View style={styles.container}>
      <Calendar
        markedDates={markedDates}
        onDayPress={day => {
          setCurrentDate(day.dateString)
        }}

        onDayLongPress={day => {
          setCurrentDate(day.dateString)
          setModalIsVisible(true)
        }}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalIsVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalIsVisible(!modalIsVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{currentDate}</Text>
            <Input
              placeholder='INPUT TITLE'
              style={styles.modalInput}
              onChangeText={(e) => {
                setCurrentNote({
                  ...currentNote,
                  date: currentDate,
                  header: e
                })
              }}
            />
            <Input
              placeholder='ADD LOCATION'
              style={styles.modalInput}
              onChangeText={(e) => {
                setCurrentNote({
                  ...currentNote,
                  date: currentDate,
                  note: e
                })
              }}
            />
            <Input
              placeholder='ADD NOTE'
              style={styles.modalInput}
              onChangeText={(e) => {
                setCurrentNote({
                  ...currentNote,
                  date: currentDate,
                  note: e
                })
              }}
            />


            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                setAllNotes([...allNotes, {
                  id: Date.now().toString() + Math.random(),
                  ...currentNote
                }])
                setMarkedDates({
                  ...markedDates,
                  [currentDate]: { selected: false, marked: true }
                })
                setModalIsVisible(!modalIsVisible)
                setCurrentNote({})
              }}
            >
              <Text style={styles.textStyle}>ADD EVENT</Text>
            </Pressable>
            <Button title="SHOW DATE PICKER" onPress={showDatePicker} />
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
              mode="time"
              date={new Date()}
              isDarkModeEnabled
            />
          </View>
        </View>
      </Modal>
      <NoteLists markedDates={markedDates} setNotes={setAllNotes} currentDate={currentDate} notes={allNotes} />
      <View style={styles.addBtnView}>
        <FAB
          icon={{ name: 'add', color: 'white' }}
          color="black"
          onPress={() => {
            setModalIsVisible(true)
          }
          }
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'space-between',
    paddingTop: 70,
    paddingBottom: 15
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 25
  },
  taskItemText: {
    paddingLeft: 10,
    paddingTop: 10
},
addBtnView: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingRight: 10
  },
  modalView: {
    margin: 20,
    width: 300,
    backgroundColor: "oldlace",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#5f9ea0",
    shadowOffset: {
    width: 0,
    height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
},
    button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
},
    buttonOpen: {
    backgroundColor: "blue",
    width: 45,
    height: 50,
    borderRadius: 50,
    paddingTop: 4,
    fontSize: 15,
    color: "#cd5c5c",
    display: "flex",
    alignItems: "center",
},
    buttonClose: {
    backgroundColor: "#cd5c5c",
},
    textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
},
    modalText: {
    marginBottom: 15,
    textAlign: "center"
},
    modalInput: {
    width: 50
  }
});